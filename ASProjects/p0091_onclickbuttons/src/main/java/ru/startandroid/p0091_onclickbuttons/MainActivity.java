package ru.startandroid.p0091_onclickbuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView myTextView;
    Button buttonOk;
    Button buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTextView = (TextView) findViewById(R.id.myTextView);
        buttonOk = (Button) findViewById(R.id.buttonOk);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);

        View.OnClickListener ocButtonOk = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTextView.setText("Нажата кнопка ОК");
            }
        };

        buttonOk.setOnClickListener(ocButtonOk);

        View.OnClickListener ocButtonCancel = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myTextView.setText("Нажата кнопка Cancel");
            }
        };

        buttonCancel.setOnClickListener(ocButtonCancel);
    }
}
