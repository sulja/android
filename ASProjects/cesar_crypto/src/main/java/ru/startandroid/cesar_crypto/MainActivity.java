package ru.startandroid.cesar_crypto;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btnEncryption;
    Button btnDecryption;
    EditText etMain;
    EditText etEncryption;
    EditText etDecryption;
    CheckBox cbCrypto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDecryption = (Button) findViewById(R.id.btnDecryption);
        btnEncryption = (Button) findViewById(R.id.btnEncryption);
        etMain = (EditText) findViewById(R.id.etMain);
        etEncryption = (EditText) findViewById(R.id.etEncryption);
        etDecryption = (EditText) findViewById(R.id.etDecryption);
        cbCrypto = (CheckBox) findViewById(R.id.cbCrypto);

        btnEncryption.setOnClickListener(this);
        btnDecryption.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDecryption:
                if(cbCrypto.isChecked()) {
                    etDecryption.setText(Square.Decryption(etEncryption.getText().toString().toUpperCase()));
                }
                else {
                    etDecryption.setText(Cesar.Decryption(etEncryption.getText().toString(), 20));
                }
                break;
            case R.id.btnEncryption:
                if(cbCrypto.isChecked()) {
                    etEncryption.setText(Square.Encryption(etMain.getText().toString().toUpperCase()));
                }
                else {
                    etEncryption.setText(Cesar.Encryption(etMain.getText().toString(), 20));
                }
                break;
        }
    }
}
