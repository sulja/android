package ru.startandroid.cesar_crypto;

public class Square {
    private static final char symbolsArray[][]  =  {{'А', 'Б', 'В', 'Г', 'Д', 'Е'},
                                                    {'Ё', 'Ж', 'З', 'И', 'Й', 'К'},
                                                    {'Л', 'М', 'Н', 'О', 'П', 'Р'},
                                                    {'С', 'Т', 'У', 'Ф', 'Х', 'Ц'},
                                                    {'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь'},
                                                    {'Э', 'Ю', 'Я', ' ', '.', ','}};


    public static String Encryption(String value)
    {
        String encryptionText = "";
        int row;
        int column;
        for(int i = 0; i < value.length(); i++)
        {
            row = findRow(value.charAt(i));
            column = findColumn(value.charAt(i));
            if(row == symbolsArray.length - 1) row = 0;
            else row++;
            encryptionText = encryptionText.concat(String.valueOf(symbolsArray[row][column]));
        }
        return  encryptionText;
    }
    public static String Decryption(String value)
    {
        String encryptionText = "";
        int row;
        int column;
        for(int i = 0; i < value.length(); i++)
        {
            row = findRow(value.charAt(i));
            column = findColumn(value.charAt(i));
            if(row == 0) row = symbolsArray.length - 1;
            else row--;
            encryptionText = encryptionText.concat(String.valueOf(symbolsArray[row][column]));
        }
        return  encryptionText;
    }

    private static int findRow(char symbol)
    {
        int index = -1;
        for(int i = 0; i < symbolsArray.length; i++)
        {
            for(int j = 0; j < symbolsArray[i].length; j++)
            {
                if(symbolsArray[i][j] == symbol)
                {
                    index = i;
                    break;
                }
            }
            if(index != -1) break;
        }
        return  index;
    }
    private static int findColumn(char symbol)
    {
        int index = -1;
        for(int i = 0; i < symbolsArray.length; i++)
        {
            for(int j = 0; j < symbolsArray[i].length; j++)
            {
                if(symbolsArray[i][j] == symbol)
                {
                    index = j;
                    break;
                }
            }
            if(index != -1) break;
        }
        return  index;
    }
}
