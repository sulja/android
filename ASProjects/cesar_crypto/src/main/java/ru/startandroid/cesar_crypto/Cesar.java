package ru.startandroid.cesar_crypto;

public class Cesar {

    private static final String ALPHABET = "аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ ";

    public static String Encryption(String value, int step)
    {
        String encryptionText = "";
        int index;
        for (int i = 0; i < value.length(); i++)
        {
            index = ALPHABET.indexOf(value.charAt(i)) - step;
            if(index < 0)
            {
                index = ALPHABET.length() + index;

            }
            encryptionText = encryptionText.concat(String.valueOf(ALPHABET.charAt(index))) ;
        }
        return  encryptionText;
    }

    public static String Decryption(String value, int step)
    {
        String decryptionText = "";
        int index;
        for (int i = 0; i < value.length(); i++)
        {
            index = ALPHABET.indexOf(value.charAt(i)) + step;
            if(index > ALPHABET.length() - 1)
            {
                index = index - ALPHABET.length();

            }
            decryptionText = decryptionText.concat(String.valueOf(ALPHABET.charAt(index))) ;
        }
        return  decryptionText;
    }
}
