package ru.startandroid.firts_task;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintSet;

import android.app.Activity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnClickListener {
    Button btnBack;
    Button btnNext;
    ImageView ivMain;
    LinearLayout imgLayout;
    ArrayList<Integer> idImages;
    ArrayList<Integer> idColors;
    int currentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnBack = (Button) findViewById(R.id.buttonBack);
        btnBack.setEnabled(false);
        btnNext = (Button) findViewById(R.id.buttonNext);
        ivMain = (ImageView) findViewById(R.id.ivMain);
        imgLayout = (LinearLayout) findViewById(R.id.imageLayout);

        idImages = getImagesArray(5);
        currentImage = 0;
        ivMain.setImageResource(idImages.get(currentImage));

        idColors = getColorsArray(5);
        imgLayout.setBackgroundResource(idColors.get(currentImage));

        btnBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);
    }

    private ArrayList<Integer> getImagesArray(int count)
    {
        ArrayList<Integer> imagesArray = new ArrayList<>();
        for(int i = 0; i < count; i++)
        {
            imagesArray.add(getResources().getIdentifier("image_first_task"+(i+1),"drawable",getPackageName()));
        }
        return imagesArray;
    }
    private ArrayList<Integer> getColorsArray(int count)
    {
        ArrayList<Integer> colorsArray = new ArrayList<>();
        for(int i = 0; i < count; i++)
        {
            colorsArray.add(getResources().getIdentifier("color"+String.valueOf(i+1),"color",getPackageName()));
        }
        return colorsArray;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.buttonBack:
                currentImage--;
                if(currentImage == 0)
                {
                    btnBack.setEnabled(false);
                }
                if(btnNext.isEnabled() == false)
                {
                    btnNext.setEnabled(true);
                }
                ivMain.setImageResource(idImages.get(currentImage));
                imgLayout.setBackgroundResource(idColors.get(currentImage));
                break;

            case R.id.buttonNext:
                currentImage++;
                if(currentImage == idImages.size() - 1)
                {
                    btnNext.setEnabled(false);
                }
                if(btnBack.isEnabled() == false)
                {
                    btnBack.setEnabled(true);
                }
                ivMain.setImageResource(idImages.get(currentImage));
                imgLayout.setBackgroundResource(idColors.get(currentImage));
                break;
        }
    }
}
