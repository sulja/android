package ru.startandroid.p0101_listener;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    Button buttonOk;
    Button buttonCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        buttonOk = (Button) findViewById(R.id.buttonOk);
        buttonCancel = (Button) findViewById(R.id.buttonCancel);

        View.OnClickListener ocButton = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId())
                {
                    case R.id.buttonOk:
                        textView.setText("Нажата кнопка ОК");
                        break;

                    case R.id.buttonCancel:
                        textView.setText("Нажата кнопка Cancel");
                        break;
                }
            }
        };

        buttonOk.setOnClickListener(ocButton);
        buttonCancel.setOnClickListener(ocButton);
    }
}
