package ru.startandroid.secondtask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class ScreenTwo extends AppCompatActivity {
    ImageView ivAnimation;
    ImageView ivMain;
    ArrayList<String> imageLinks;
    Handler myHandler;
    Thread myThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_two);

        ivMain      = (ImageView) findViewById(R.id.ivMain);
        ivAnimation = (ImageView) findViewById(R.id.ivAnimation);
        ivAnimation.setImageResource(R.drawable.cirlce);
        imageLinks = readLinks(R.raw.links);

        myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int currentImage = 0;
                changeImage(currentImage);
            }
        });

        myHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                Picasso.get()
                        .load(imageLinks.get(msg.what))
                        .placeholder(R.drawable.load)
                        .error(R.drawable.cirlce)
                        .into(ivMain);
            };
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        animation();
        if(imageLinks.size() > 0)
        {
            myThread.start();
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        myThread.stop();
    }
    private void animation() {
        ivAnimation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.rotate_iv_animation));
    }
    private ArrayList<String> readLinks(int idFile)
    {
        ArrayList<String> links = new ArrayList<String>();

        InputStream inputStream = getResources().openRawResource(idFile);
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        try {
            String line = bufferedReader.readLine();
            while (line != null) {
                links.add(line);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return links;
    }
    private void changeImage(int currentImage)
    {
        if(currentImage == imageLinks.size())
        {
            currentImage = 0;
        }
        else
        {
            myHandler.sendEmptyMessage(currentImage);
            currentImage++;
            try {
                TimeUnit.SECONDS.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        changeImage(currentImage);
    }
}