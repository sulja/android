package ru.startandroid.secondtask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;


public class MainActivity extends AppCompatActivity {
    TextView tvAnimation;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvAnimation = (TextView) findViewById(R.id.tvAnimation);
    }

    @Override
    protected void onResume() {
        super.onResume();
        tvAnimation.startAnimation(AnimationUtils.loadAnimation(this, R.anim.start_animation));
        intent = new Intent(this, ScreenTwo.class);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(6);
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
